# Sockets

### Compilar

Para compilar o código referente a cada questão, basta seguir os passos abaixo:

```sh
$ cd Q0X
$ make
```

### Executar

Q01:

```sh
$ ./client <servidor> <mensagem1> ... <mensagemN>
$ ./server
```

Q02:

```sh
$ ./client <servidor> <mensagem1> ... <mensagemN>
$ ./server
```

Q03:

```sh
$ ./client <servidor>
$ ./server
```
