#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */
#include <time.h>
#include <ctime> // Imprimir o horário

#include <iostream>
#include <cstdlib>

#define LOCAL_SERVER_PORT 1500
#define MAX_MSG 100

using namespace std;

int main(int argc, char *argv[]) {

  int sd, rc, n;
  socklen_t cliLen;
  struct sockaddr_in cliAddr, servAddr;
  char msg[MAX_MSG];

  /* 1. CRIA O SOCKET */
  sd=socket(AF_INET, SOCK_DGRAM, 0);
  if(sd<0) {
    cout << argv[0] << ": nao foi possivel abrir o socket" << endl;
    exit(1);
  }

  /* 2. FAZ A LIGAÇÃO (BIND) DE TODOS OS ENDEREÇOS DO SERVIDOR COM A PORTA */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(LOCAL_SERVER_PORT);

  rc = bind (sd, (struct sockaddr *) &servAddr,sizeof(servAddr));

  if(rc<0) {
    cout << argv[0] << ": nao foi possivel associar a porta" << LOCAL_SERVER_PORT << endl;
    exit(1);
  }

  cout << argv[0] << ": aguardando por dados na porta UDP(" << LOCAL_SERVER_PORT << ")" << endl;

  /* 3. LOOP INFINITO PARA LEITURA (LISTEN) */
  while(1) {

    /* INICIA O BUFFER DE COMUNICAÇÃO */
    memset(msg,0x0,MAX_MSG);


    /* RECEBE MENSAGEM */
    cliLen = sizeof(cliAddr);
    n = recvfrom(sd, msg, MAX_MSG, 0, (struct sockaddr *) &cliAddr, &cliLen);

    if(n<0) {
      cout << argv[0] << ": nao foi possivel receber dados" << endl;
      continue;
    } else {
      /* 4. RETORNA MENSAGEM PARA O CLIENTE */

      /* HORÁRIO DO SISTEMA */
      time_t rawtime;
      struct tm * timeinfo;
      char formatedTime[40];

      time(&rawtime);
      timeinfo = localtime(&rawtime);
      strftime(formatedTime, 40, "%Ih%M", timeinfo);

      /* FORMATA MENSAGEM DE RESPOSTA */
      string strMsg(msg);
      string strFormatedTime(formatedTime);
      string str = "Mensagem '" + strMsg + "' recebida às " + strFormatedTime + " (hora local)";
      const char * responseMessage = str.c_str();

      /* ENVIA MENSAGEM PARA O CLIENTE */
      rc = sendto(sd, responseMessage, strlen(responseMessage)+1, 0,
      (struct sockaddr *) &cliAddr,
      sizeof(cliAddr));
    }

    /* IMPRIME A MENSAGEM RECEBIDA */
    cout << argv[0] << ": de " << inet_ntoa(cliAddr.sin_addr) << ":UDP(" << ntohs(cliAddr.sin_port) << ") : " << msg << endl;

  }/* FIM DO LOOP INFINITO */

return 0;

}
