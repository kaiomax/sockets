#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h> /* memset() */
#include <sys/time.h> /* select() */

#include <iostream>
#include <cstdlib>

#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100

using namespace std;

int main(int argc, char *argv[]) {

  int clientSkt, vRcv;
  struct sockaddr_in serverAddr;
  struct hostent *h;
  char msg[MAX_MSG];
  bool isRunning = true;
  string strMsg;

  /* VERIFICA OS ARGUMENTOS PASSADOS POR LINHA DE COMANDO */
  if(argc<2) {
    cout << "Uso : " << argv[0] << " <servidor> <mensagem1> ... <mensagemN>" << endl;
    exit(1);
  }

  /* OBTEM O ENDEREÇO IP e PESQUISA O NOME NO DNS */
  h = gethostbyname(argv[1]);
  if(h==NULL) {
    cout << argv[0] << ": host desconhecido " << argv[1] << endl;
    exit(1);
  }

  cout << argv[0] << ": enviando dados para " << h->h_name << " (IP : " << inet_ntoa(*(struct in_addr *)h->h_addr_list[0]) << ")" << endl;

  /* CONFIGURANDO ESTRUTURA REFERENTE AO HOST REMOTO (SERVIDOR) */
  serverAddr.sin_family = h->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
  serverAddr.sin_port = htons(REMOTE_SERVER_PORT);

  cout << "Digite 'sair' para encerrar a conexão." << endl;

  while(isRunning) {
    /* 1. CRIA O SOCKET */
    clientSkt = socket(AF_INET, SOCK_STREAM, 0);
    if(clientSkt < 0) {
      cout << argv[0] << ": nao foi possivel abrir o socket" << endl;
      exit(1);
    }

    /* 2. INICIA A CONEXÃO ENTRE O CLIENTE E O SERVIDOR (CONNECT) */
    vRcv = connect(clientSkt, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
    if(vRcv < 0) {
      cout << "Não foi possível estabelecer a conexão com o servidor." << endl;
      exit(1);
    }

    cout << "Mensagem: ";
    getline(cin, strMsg);

    if(strMsg == "sair") {
      isRunning = false;
      continue;
    } else {
      /* 3. ENVIA DADOS  (SEND) */
      send(clientSkt, strMsg.c_str(), MAX_MSG, 0);
    }

    /* 4. RECEBE RESPOSTA DO SERVER (RECV) */
    recv(clientSkt, msg, MAX_MSG, 0);

    cout << msg << endl;
    cout << endl;
  }

  /* 5. ENCERRA O SOCKET */
  close(clientSkt);

  return 1;
}
