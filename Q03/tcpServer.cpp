#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */
#include <time.h>
#include <ctime> // Imprimir o horário
#include <iostream>
#include <cstdlib>
#include <thread>

#define LOCAL_SERVER_PORT 1500
#define MAX_MSG 100
#define BACKLOG 10

using namespace std;

void printSomeValues(int val, char* str, double dval) {
  cout << val << " " << str << " " << dval << endl;
}

void threadFunc(int number) {
  cout << "Welcome to Multithreading" << number << endl;
}

void serverClient(int clientSkt, char * argFile) {
  char msg[MAX_MSG];
  int vRcv;

  if(clientSkt < 0) {
    cout << "Erro ao estabelecer conexão com o cliente." << endl;
    exit(1);
  } else {
    /* INICIA O BUFFER DE COMUNICAÇÃO */
    memset(msg, 0x0, MAX_MSG);

    /* 4. RECEBE MENSAGEMS (RECV) */
    vRcv = recv(clientSkt, msg, MAX_MSG, 0);
    if(vRcv < 0) {
      cout << argFile << ": nao foi possivel receber dados" << endl;
      exit(1);
    }

    if (vRcv != 0) {
      /* IMPRIME A MENSAGEM RECEBIDA */
      cout << argFile << ": Mensagem recebida -> " << msg << endl;

      /* 5. RETORNA MENSAGEM PARA O CLIENTE (SEND) */

      /* HORÁRIO DO SISTEMA */
      time_t rawtime;
      struct tm * timeinfo;
      char formatedTime[40];

      time(&rawtime);
      timeinfo = localtime(&rawtime);
      strftime(formatedTime, 40, "%Ih%M", timeinfo);

      /* FORMATA MENSAGEM DE RESPOSTA */
      string strMsg(msg);
      string strFormatedTime(formatedTime);
      string str = "Mensagem '" + strMsg + "' recebida às " + strFormatedTime + " (hora local)";
      const char * responseMessage = str.c_str();

      /* ENVIA MENSAGEM PARA O CLIENTE */
      send(clientSkt, responseMessage, strlen(responseMessage)+1, 0);
    }

    close(clientSkt);
  }
}

int main(int argc, char *argv[]) {
  int serverSkt, clientSkt, vRcv;
  socklen_t clientAddrSize;
  struct sockaddr_in clientAddr, serverAddr;

  /* 1. CRIA O SOCKET */
  serverSkt = socket(AF_INET, SOCK_STREAM, 0);
  if(serverSkt < 0) {
    cout << argv[0] << ": nao foi possivel abrir o socket" << endl;
    exit(1);
  }

  /* 2. FAZ A LIGAÇÃO (BIND) DE TODOS OS ENDEREÇOS DO SERVIDOR COM A PORTA */
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serverAddr.sin_port = htons(LOCAL_SERVER_PORT);

  vRcv = ::bind(serverSkt, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

  if(vRcv < 0) {
    cout << argv[0] << ": nao foi possivel associar a porta " << LOCAL_SERVER_PORT << endl;
    exit(1);
  }

  cout << argv[0] << ": aguardando por dados na porta TCP(" << LOCAL_SERVER_PORT << ")" << endl;

  /* 3. COLOCAR O SOCKET EM MODO DE ESCUTA (LISTEN) */
  listen(serverSkt, BACKLOG);

  clientAddrSize = sizeof clientAddr;

  while(1) {
    /* 3. ACEITA PEDIDOS DE CONEXÃO (ACCEPT) */
    clientSkt = accept(serverSkt, (struct sockaddr *) &clientAddr, &clientAddrSize);

    thread t(serverClient, clientSkt, argv[0]);
    t.detach();
  }/* FIM DO LOOP INFINITO */

  return 1;
}
