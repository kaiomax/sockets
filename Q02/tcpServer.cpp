#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */
#include <time.h>
#include <ctime> // Imprimir o horário

#include <iostream>
#include <cstdlib>

#define LOCAL_SERVER_PORT 1500
#define MAX_MSG 100
#define BACKLOG 10

using namespace std;

int main(int argc, char *argv[]) {

  int sd, rc, n, cd;
  socklen_t size;
  struct sockaddr_in cliAddr, servAddr;
  char msg[MAX_MSG];
  struct sockaddr_storage theirAddr;

  /* 1. CRIA O SOCKET */
  sd = socket(AF_INET, SOCK_STREAM, 0);
  if(sd < 0) {
    cout << argv[0] << ": nao foi possivel abrir o socket" << endl;
    exit(1);
  }

  /* 2. FAZ A LIGAÇÃO (BIND) DE TODOS OS ENDEREÇOS DO SERVIDOR COM A PORTA */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(LOCAL_SERVER_PORT);

  rc = bind(sd, (struct sockaddr *) &servAddr, sizeof(servAddr));

  if(rc<0) {
    cout << argv[0] << ": nao foi possivel associar a porta " << LOCAL_SERVER_PORT << endl;
    exit(1);
  }

  cout << argv[0] << ": aguardando por dados na porta TCP(" << LOCAL_SERVER_PORT << ")" << endl;

  /* 3. COLOCAR O SOCKET EM MODO DE ESCUTA (LISTEN) */
  listen(sd, BACKLOG);

  size = sizeof cliAddr;

  while(1) {
    /* 3. ACEITA PEDIDOS DE CONEXÃO (ACCEPT) */
    cd = accept(sd, (struct sockaddr *) &cliAddr, &size);

    if(cd < 0) {
      cout << "Erro ao estabelecer conexão com o cliente." << endl;
      exit(1);
    } else {
      /* INICIA O BUFFER DE COMUNICAÇÃO */
      memset(msg,0x0,MAX_MSG);

      /* 4. RECEBE MENSAGEMS (RECV) */
      n = recv(cd, msg, MAX_MSG, 0);
      if(n < 0) {
        cout << argv[0] << ": nao foi possivel receber dados" << endl;
        continue;
      }

      /* IMPRIME A MENSAGEM RECEBIDA */
      cout << argv[0] << ": Mensagem recebida -> " << msg << endl;

      /* 5. RETORNA MENSAGEM PARA O CLIENTE (SEND) */

      /* HORÁRIO DO SISTEMA */
      time_t rawtime;
      struct tm * timeinfo;
      char formatedTime[40];

      time(&rawtime);
      timeinfo = localtime(&rawtime);
      strftime(formatedTime, 40, "%Ih%M", timeinfo);

      /* FORMATA MENSAGEM DE RESPOSTA */
      string strMsg(msg);
      string strFormatedTime(formatedTime);
      string str = "Mensagem '" + strMsg + "' recebida às " + strFormatedTime + " (hora local)";
      const char * responseMessage = str.c_str();

      /* ENVIA MENSAGEM PARA O CLIENTE */
      send(cd, responseMessage, strlen(responseMessage)+1, 0);
    }

    /* 6. ENCERRA O SOCKET (CLOSE) */
    close(cd);
  }/* FIM DO LOOP INFINITO */

  return 0;

}
